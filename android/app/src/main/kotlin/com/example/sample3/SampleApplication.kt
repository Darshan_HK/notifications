package com.example.sample3

import com.moengage.core.LogLevel
import com.moengage.core.MoEngage
import com.moengage.core.MoEngage.Builder
import com.moengage.flutter.MoEInitializer
import com.moengage.pushbase.MoEPushHelper
import io.flutter.app.FlutterApplication


class SampleApplication : FlutterApplication() {
    override
    fun onCreate() {
        super.onCreate()
        val builder: MoEngage.Builder = Builder(this, "T1RS2D8IG8IU5BKUCYH8TG9O")
        MoEInitializer.initialize(getApplicationContext(), builder)
        // optional, required in-case notification customisation is required.
        MoEPushHelper.getInstance().setMessageListener(CustomPushListener())
    }
}

