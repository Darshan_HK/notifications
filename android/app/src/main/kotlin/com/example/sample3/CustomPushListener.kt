package com.example.sample3

import android.app.Activity
import android.content.Context
import android.os.Bundle
import com.moengage.core.Logger
import com.moengage.plugin.base.PluginPushCallback

/**
 * @author Umang Chamaria
 * Date: 2020/12/06
 */
class CustomPushListener : PluginPushCallback() {
    override
    fun onHandleRedirection(activity: Activity, payload: Bundle) {
        super.onHandleRedirection(activity, payload)
        Logger.v(TAG + " onHandleRedirection() : ")
    }


   override
    fun onNotificationReceived(context: Context?, payload: Bundle?) {
        super.onNotificationReceived(context, payload)
        Logger.v(TAG + " onNotificationReceived() : ")
    }

    companion object {
        private const val TAG = "CustomPushListener"
    }
}